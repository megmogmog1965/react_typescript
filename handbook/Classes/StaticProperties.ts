
class Grid {
  static origin: Point = { x: 0, y: 0 };

  constructor(public scale: number) { }

  calculateDistanceFromOrigin(point: { x: number, y: number }): number {
    var xDist = point.x - Grid.origin.x;
    var yDist = point.y - Grid.origin.y;

    return Math.sqrt(Math.pow(xDist, 2) + Math.pow(yDist, 2)) / this.scale;
  }
}


class Point {
  constructor(public x: number, public y: number) { }
}


var grid1 = new Grid(1.0);  // 1x scale
var grid2 = new Grid(5.0);  // 5x scale

console.log(grid1.calculateDistanceFromOrigin({ x: 10, y: 10 }));
console.log(grid2.calculateDistanceFromOrigin({ x: 10, y: 10 }));
