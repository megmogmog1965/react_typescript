declare var require;

import validation = require('../Export/Validation');
import zipValidator = require('../Export/ZipCodeValidator');
import lettersValidator = require('../Export/LettersOnlyValidator');

// Some samples to try
var strings = ['Hello', '98052', '101'];

// Show whether each string passed each validator
strings.forEach(s => {
  // lazy loading.
  var zip: typeof zipValidator = require('../Export/ZipCodeValidator');
  var letters: typeof lettersValidator = require('../Export/LettersOnlyValidator');

  // Validators to use
  var validators: { [s: string]: validation.StringValidator; } = {};
  validators['ZIP code'] = new zip();
  validators['Letters only'] = new letters();

  for (var name in validators) {
    console.log('"' + s + '" ' + (validators[name].isAcceptable(s) ? ' matches ' : ' does not match ') + name);
  }
});
