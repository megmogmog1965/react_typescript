//// implementing ////

interface ClockInterface {
  currentTime: Date;
}

class Clock implements ClockInterface {

  currentTime: Date;

  constructor(h: number, m: number) {
    // nothing to be done.
  }

  setTime(d: Date): void {
    this.currentTime = d;
  }
}


//// extending ////

interface Shape {
  color: string;
}

interface PenStroke {
  penWidth: number;
}

interface Square extends Shape, PenStroke {
  sideLength: number;
}

var square = <Square>{};
square.color = 'blue';
square.sideLength = 10;
square.penWidth = 5.0;


//// hybrid types ////

interface Counter {
  (start: number): string;
  interval: number;
  reset(): void;
}

// var c: Counter;
// c(10);
// c.reset();
// c.interval = 5.0;
