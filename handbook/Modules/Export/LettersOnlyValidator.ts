
import validation = require('./Validation');

var lettersRegexp = /^[A-Za-z]+$/;

class LetterOnlyValidator implements validation.StringValidator {
  isAcceptable(s: string) {
    return lettersRegexp.test(s);
  }
}

export = LetterOnlyValidator
