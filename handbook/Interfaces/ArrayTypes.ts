
interface StringArray {
  [index: number]: string;
}

interface Dictionary {
  [index: string]: string;
}

//// Array<String> ////
var array: StringArray = [ 'hello', 'world' ];

console.log(array);
console.log(array[0]);

//// Dictionary<String> ////
var dict: Dictionary = { 'a': 'aaaa', 'b': 'bbbb' };

console.log(dict);
console.log(dict['a']);
