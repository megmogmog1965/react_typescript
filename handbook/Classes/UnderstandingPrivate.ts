class AA {
  private name: string;
  constructor(theName: string) { this.name = theName; }
}

class SubAA extends AA {
  constructor() { super('SubAA'); }
}

class BB {
  private name: string;
  constructor(theName: string) { this.name = theName; }
}

var aa = new AA('Goat');
var subaa = new SubAA();
var bb = new BB('Bob');
aa = subaa;
// aa = bb;  // An error occurs.
