
class Animal {
  private _name: string;

  constructor(name: string) {
    this._name = name;
  }

  move(meters: number = 0): void {
    console.log(this._name + ' moved ' + meters + 'm.');
  }
}

class Snake extends Animal {
  constructor(name: string) {
    super(name);
  }

  move(meters: number = 0): void {
    console.log('Slithering...');
    return super.move(meters);
  }
}

class Horse extends Animal {
  constructor(name: string) {
    super(name);
  }

  move(meters: number = 45): void {
    console.log('Galloping...');
    super.move(meters);
  }
}


var sam = new Snake('Sammy the Python');
var tom: Animal = new Horse('Tommy the Palomino');

sam.move();
tom.move(34);
