
interface SearchFunc {
  (source: string, subString: string): boolean;
}


var mySearch: SearchFunc;
mySearch = function(source: string, sub: string): boolean {
  var result: number = source.search(sub);

  return (result === -1) ? false : true;
};

console.log(mySearch);
