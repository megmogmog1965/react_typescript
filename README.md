# What's this ?

[TypeScript] + [React] + [Atom] のお試し.

# References

## Atom

* [AtomでTypeScriptの環境を構築する]
* [Atom Runner]
* [Atom - Minimap]

## Node.js

* [node.js installation with nodebrew]

## TypeScript

* [React JSX with TypeScript]

## React

* ``@todo``


[TypeScript]:http://www.typescriptlang.org/Handbook
[React]:https://facebook.github.io/react/
[Atom]:https://atom.io/

[node.js installation with nodebrew]:http://qiita.com/n0bisuke/items/74f6bd05eec2f9986ee6
[React JSX with TypeScript]:http://qiita.com/Quramy/items/70f97e68d21859d91ed8
[AtomでTypeScriptの環境を構築する]:http://qiita.com/Sugima/items/113eb16f14ca1e9a6d0f
[Atom Runner]:http://qiita.com/__mick/items/3ca273e158c3a32dbb92
[Atom - Minimap]:https://atom.io/packages/minimap
[Atom - PATH]:http://qiita.com/rei-m/items/0af8260307bba2845c8a
