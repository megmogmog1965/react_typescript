
class Employee {
  private _fullName: string;

  get fullName() {
    return this._fullName;
  }

  set fullName(newName: string) {
    this._fullName = newName;
  }
}

var employee = new Employee();
employee.fullName = 'Bob Smith';
if (employee.fullName) {
  console.log(employee.fullName);
}
