
var isDone: boolean = false;
var height: number = 6.0;
var name: string = 'bob';

var list: Array<number> = [ 1, 2, 3 ];

enum Color { Red=1, Green, Blue };
var c: Color = Color.Green;
var colorName: string = Color[2];

console.log(colorName);


var notSure: any = 4;
notSure = 'maybe a string instead';
notSure = false;

var anyList: Array<any> = [ 1, 'hello', false ];


function warnUser(): void {
  console.log('This is my warning message.');
}

warnUser();
